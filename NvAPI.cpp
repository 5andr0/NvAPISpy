// NvAPI.cpp : Defines the exported functions for the DLL application.
// This DLL project is a mock implementation of the NVidia NvAPI for spying on NvAPI calls
// It should also be able to forward calls to the real NvAPI

#include <Windows.h>
#include "nvapi.h"
#include <fstream>
#include <string>
#include <locale>
#include <codecvt>
#include <sstream>
#include <iomanip>
#include <chrono>
#include <ctime>

#ifdef _WIN64
#define ARCH "64"
#else
#define ARCH ""
#endif

// Location of real NvAPI DLL files
#define NVAPIDLL_1      "C:\\Windows\\System32\\nvapi" ARCH "_orig.dll"
#define NVAPIDLL_2      "C:\\Windows\\System32\\nvapi" ARCH ".dll"

#define NVAPI_ERROR 1

// Pointers to real NvAPI SDK DLL functions
typedef NV_STATUS  (*NVAPI_INITIALIZE)(void);
typedef void*      (*NVAPI_QUERYINTERFACE)(unsigned int function_code);
typedef NV_STATUS  (*NVAPI_I2CWRITEEX)(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_I2C_INFO_V3* i2c_info, NV_U32* unknown);
typedef NV_STATUS  (*NVAPI_I2CREADEX)(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_I2C_INFO_V3* i2c_info, NV_U32* unknown);

// Module pointer for real NvAPI
HMODULE hModule = NULL;

NVAPI_INITIALIZE        _NvAPI_Initialize;
NVAPI_QUERYINTERFACE    _NvAPI_QueryInterface;
NVAPI_I2CWRITEEX        _NvAPI_I2CWriteEx;
NVAPI_I2CREADEX         _NvAPI_I2CReadEx;

class logger {
    std::ofstream outfile;
    std::string executable;

    std::string get_executable() {
        for (unsigned i = 0; i < 8; i++) {
            std::wstring p(std::size_t(MAX_PATH) << i, L'\0'); // using unicode to enable long paths
            std::size_t size = GetModuleFileNameW(nullptr, &p[0], MAX_PATH << i);

            switch (GetLastError()) {
            case 0:
                p.resize(size);
                return std::wstring_convert<std::codecvt_utf8<wchar_t>>().to_bytes(
                    p.substr((size = p.find_last_of(L"/\\")) != std::string::npos ? size + 1 : 0)
                    );
            case ERROR_INSUFFICIENT_BUFFER:
                continue;
            }
        }
        return "";
    }

    logger() {
        std::time_t t = std::time(nullptr);
        std::tm tm;
        localtime_s(&tm, &t);
        std::ostringstream oss;
        executable = get_executable();
        oss << "C:\\NvAPISpy\\nvapi" ARCH "_" << executable << "_" << std::put_time(&tm, "%Y-%m-%d_%H-%M-%S") << ".txt";
        outfile.open(oss.str().c_str(), std::ofstream::out);
    }

public:
    void log(const NV_I2C_INFO_V3* i2c_info, const char* fn_prefix) {
        if (!i2c_info) return;

        auto now = std::chrono::system_clock::now();
        auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()) % 1000;
        auto timer = std::chrono::system_clock::to_time_t(now);
        std::tm tm;
        localtime_s(&tm, &timer);

        std::ostringstream oss;
        oss << fn_prefix << " " << std::put_time(&tm, "%H:%M:%S") << '.';
        oss << std::setfill('0') << std::setw(3) << ms.count();

        oss << " Dev: 0x" << std::hex << std::setw(2) << (i2c_info->i2c_dev_address >> 1);

        oss << " Reg(" << std::dec << i2c_info->reg_addr_size << "):" << std::hex;
        for (decltype(i2c_info->reg_addr_size) i = 0; i < i2c_info->reg_addr_size; i++)
            oss << " " << std::setw(2) << (unsigned)i2c_info->i2c_reg_address[i];

        oss << " Data(" << std::dec << i2c_info->size << "):" << std::hex;
        for (decltype(i2c_info->size) i = 0; i < i2c_info->size; i++)
            oss << " " << std::setw(2) << (unsigned)i2c_info->data[i];

        oss << std::endl;
        outfile << oss.str();
        outfile.flush();
        // https://docs.microsoft.com/en-us/sysinternals/downloads/debugview
        OutputDebugStringA(std::string(executable + "::" + oss.str()).c_str());
    }

    static logger& get() { // this is thread safe since c++11
        static logger instance;
        return instance;
    }
};

extern "C"
{
    __declspec(dllexport) void* nvapi_QueryInterface(unsigned int function_code)
    {
        void* ret_val = NULL;

        printf("nvapi_QueryInterface\r\n");
        if (!hModule)
        {
            hModule = LoadLibraryA(NVAPIDLL_1);

            if (!hModule)
            {
                hModule = LoadLibraryA(NVAPIDLL_2);
            }

            if (hModule)
            {
                _NvAPI_QueryInterface = (NVAPI_QUERYINTERFACE)GetProcAddress(hModule, "nvapi_QueryInterface");

                _NvAPI_I2CWriteEx     = (NVAPI_I2CWRITEEX)_NvAPI_QueryInterface(0x283AC65A);
                _NvAPI_I2CReadEx      = (NVAPI_I2CREADEX)_NvAPI_QueryInterface(0x4D7B0709);
            }
        }

        if (hModule)
        {
            // Get actual value
            ret_val = _NvAPI_QueryInterface(function_code);

            // Override if it's a function we fake
            switch (function_code)
            {
            case 0x283AC65A:
                ret_val = &NvAPI_I2CWriteEx;
                break;

            case 0x4D7B0709:
                ret_val = &NvAPI_I2CReadEx;
                break;
            }
        }

        return(ret_val);
    }
}

NV_STATUS NvAPI_I2CWriteEx(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_I2C_INFO_V3* i2c_info, NV_U32* unknown)
{
    NV_STATUS ret_val = NVAPI_ERROR;

    if(hModule)
    {
        logger::get().log(i2c_info, "NvAPI_I2CWriteEx:");
        ret_val = _NvAPI_I2CWriteEx(physical_gpu_handle, i2c_info, unknown);
    }

    return(ret_val);
}

NV_STATUS NvAPI_I2CReadEx(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_I2C_INFO_V3* i2c_info, NV_U32* unknown)
{
    NV_STATUS ret_val = NVAPI_ERROR;

    if (hModule)
    {
        ret_val = _NvAPI_I2CReadEx(physical_gpu_handle, i2c_info, unknown);
        logger::get().log(i2c_info, "NvAPI_I2CReadEx: ");
    }

    return(ret_val);
}